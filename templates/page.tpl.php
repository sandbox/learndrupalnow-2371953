<?php
/**
 * @file
 * Respo Bootstrap 3 Theme page.tpl.php.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first_front']: Items for the first sidebar.
 * - $page['sidebar_second_front']: Items for the second sidebar.
 * - $page['sidebar_third_front']: Items for the header region.
 * - $page['sidebar_right']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<header class="navbar">
    <div id="myHeader" class="container">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#myMainNav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <?php if ($logo): ?>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="myLogo" class="navbar-brand">
              </a>
            <?php endif; ?>                
            
            <?php if ($site_slogan): ?>
              <br /><br />
              <div id="mySlogan" class="visible-lg"><?php print $site_slogan; ?></div>
            <?php endif; ?>          
        </div>
        <nav id="myMainNav" class="collapse navbar-collapse">
            <?php if ($main_menu): ?>
              <div id="navigation">
                <div class="section">
                <?php print theme('links__system_main_menu', array(
                        'links' => $main_menu,
                        'attributes' => array(
                          'id' => 'main-menu',
                          'class' => array(
                            'nav',
                            'navbar-nav',
                            'navbar-right'
                          )
                        ),
                        'heading' => '')
                        );
                ?>                
                </div>
              </div> <!-- /.section, /#navigation -->
            <?php endif; ?>
        </nav>
    </div>
</header>

<?php if($is_front):?>
<section id="mySlider" class="visible-sm visible-md visible-lg">
    <div class="container">
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="height: 400px; margin-bottom: 10px">  
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>  
                    <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>                      
                </ol>
                <div class="carousel-inner"> 
                    <div class="item active">
                        <img src="<?php print (path_to_theme()); ?>/images/slider1.png" alt="slider 1" />
                    </div>    
                    <div class="item">
                        <img src="<?php print (path_to_theme()); ?>/images/slider2.png" alt="slider 2" />
                    </div>  
                    <div class="item">
                        <img src="<?php print (path_to_theme()); ?>/images/slider1.png" alt="slider 3" />
                    </div>                       
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</section>

<section id="my3Blocks">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-4 col-lg-4">              
                <?php if ($page['sidebar_first_front']): ?>
                  <?php print render($page['sidebar_first_front']); ?>
                <?php endif; ?>                            
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <?php if ($page['sidebar_second_front']): ?>
                  <?php print render($page['sidebar_second_front']); ?>
                <?php endif; ?>                                
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <?php if ($page['sidebar_third_front']): ?>
                  <?php print render($page['sidebar_third_front']); ?>
                <?php endif; ?>                             
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section id="myContent">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-8 col-lg-8">
                <div>
                    <br />
                    <?php if ($breadcrumb): ?>
                      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
                    <?php endif; ?>
                
                    <?php print $messages; ?>
                
                    <div id="content" class="column"><div class="section">
                      <a id="main-content"></a>
                      <?php print render($title_prefix); ?>
                      <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
                      <?php print render($title_suffix); ?>
                      <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
                      <?php print render($page['help']); ?>
                      <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
                      <?php print render($page['content']); ?>
                      <?php print $feed_icons; ?>
                    </div></div> <!-- /.section, /#content -->
                
                    <?php if($is_front):?>
                    <h2>WHAT IS OUR IDEA? WHAT IS OUR MISSION?<br /> WHAT IS OUR OPINION?</h2>
                    <img src="<?php print (path_to_theme()); ?>/images/Bottom.jpg" alt="" class="img-thumbnail pull-left" style="margin-right: 10px;" />
                    <div>
                        Our company comes to trade few years ago but from beginning we start to do only professional projects.
                        We our not satisfied with just good result. It needs to be perfect.
                        <br /><br />
                        That's why we teach our employers still to make them the best in this region.
                        Every 2 years we give to our employers test from new technologies to make sure that we are leaders on market.
                        <br /><br />
                        There is no excuse for fails. Our standards are so high that for us is every time the first rule quality.
                        <br /><br />
                        You can see in photogallery our references but they are more. Our company are in touch with partners like QINT, TSN-D or FinishLine...
                        <br /><br />
                        Come to our company take prospects to see if our portfolio fit to your vision and after that we can find the best way
                        how to realize it TOGETHER!
                        <br /><br />
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <?php if ($page['sidebar_right']): ?>
                  <?php print render($page['sidebar_right']); ?>
                <?php endif; ?>                                            
            </div>
        </div>
    </div>
</section>

<section id="myFooterLinks">
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-9 col-lg-9">
                <div>
                    <h4>QUICK CONTACT</h4>
                    <i class="glyphicon glyphicon-envelope"></i> info@constructioncompany.com
                    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                    <i class="glyphicon glyphicon-phone-alt"></i> +445 859 898 9811
                    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                    <i class="glyphicon glyphicon-globe"></i> www.learn-drupal.com
                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <div class="pull-right">
                    <h4>SOCIAL LINKS</h4>
                    <a class="btn btn-sm btn-social-icon btn-facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="btn btn-sm btn-social-icon btn-twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a class="btn btn-sm btn-social-icon btn-google-plus">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a class="btn btn-sm btn-social-icon btn-flickr">
                        <i class="fa fa-flickr"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-12">
                <hr />
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <?php if ($main_menu): ?>
                  <div id="navigation">
                    <div class="section">
                      <ul id="footer-menu" class="list-inline pull-left">
                        <li>Facebook</li>
                        <li>Google+</li>
                        <li>Twitter</li>
                      </ul>                                      
                    </div>
                  </div> <!-- /.section, /#navigation -->
                <?php endif; ?>
                <span class="pull-right">&copy; 2014 Learn-Drupal.com</span>
            </div>
        </div>
    </div>
</section>
