<?php
/**
 * @file
 * Respo Bootstrap 3 Theme more link override.
 *
 * Available variables:
 * - $more_url: Path of link.
 * - $link_text: Text of link.
 */
?>

<div class="more-link">
  <a href="<?php print $more_url ?>" class="btn btn-default">
    <?php print $link_text; ?>
  </a>
</div>
