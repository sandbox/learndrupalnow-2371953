-- SUMMARY --

The RESPO Bootstrap 3 theme is very simple bootstrap 3 theme.

For a full description of the theme, visit the project page:
  http://drupal.org/project/admin_menu
  http://www.learn-drupal.com


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Standard theme installation.


-- CONFIGURATION --

* Configure blocks in Structure » Blocks:

  - 3 blocks on front page

  - 1 block - right sidebar
  
  - 1 block - main content
  

-- CUSTOMIZATION --

* Logo you can override in css file css/mystyles.css on line 14 in ID #myLogo
* Slider you can adjust in templates/page.tpl.php - ID #mySlider


-- TROUBLESHOOTING --

* If you slider is not showing check paths in templates/page.tpl.php 


-- CONTACT --

Current maintainers:
* Anton - http://www.learn-drupal.com
